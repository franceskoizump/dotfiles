call plug#begin('~/.vim/plugged')
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'rip-rip/clang_complete'
Plug 'junegunn/fzf.vim'

call plug#end()

map <F2> :Files<CR>
